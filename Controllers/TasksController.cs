using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using net_core_api.Models;

namespace net_core_api.Controllers
{
    [ApiController]
    [Route("api/tasks")]
    public class TasksController : ControllerBase
    {
        private static List<User> users = new List<User>();

        [HttpPost("AddUserWithTask")]
        public IActionResult AddUserWithTask(User user)
        {
            users.Add(user);
            return Ok();
        }

        [HttpGet("GetUserWithTask")]
        public IActionResult GetUserWithTask(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return Ok(users);
            }
            else
            {
                var filteredUsers = users.FindAll(u => u.Name.ToLower() == name.ToLower());
                return Ok(filteredUsers);
            }
        }
    }
}
